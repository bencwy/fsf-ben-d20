// Handles API routes

module.exports = function (app, db) {
    // var <Model> = require('./api/<model>.controller')(db);
    var Grocery_list = require('./api/grocery_list.controller')(db);

    // REST API from client to server
    // app.<method>("/api/<model>", <Model>.<function>);
    app.get("/api/grocery_list", Grocery_list.retrieve);

    app.get("/api/grocery_list_one", Grocery_list.retrieveOne);

    app.post("/api/grocery_list", Grocery_list.updateOne);

};