// Read configurations
var config = require('./config');

// Load Sequelize package
var Sequelize = require("sequelize");

// Create Sequelize DB connection
var sequelize = new Sequelize(
    'shop',
    config.MYSQL_USERNAME,
    config.MYSQL_PASSWORD,
    {
        host: config.MYSQL_HOSTNAME,
        port: config.MYSQL_PORT,
        logging: config.MYSQL_LOGGING,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000,
        },
    }
);

// Import DB Models
// const <Model> = sequelize.import('./models/<model>');
const Grocery_list = sequelize.import('./models/grocery_list');

// Define Model Associations
// <Model>.hasMany(<Model>, { foreignKey: '<field name>' });
// <Model>.belongsTo(<Model>, { foreignKey: '<field name>' });

// Exports Models
module.exports = {
    // Loads model for departments table
    // <Model>: <Model>,
    Grocery_list: Grocery_list,
    sequelize: sequelize
};