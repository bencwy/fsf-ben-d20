var retrieve = function (db) {
    return function (req, res) {
        // Sequelize db calls OR Upload OR normal functions
        console.log("Retrieving List");
        console.log(req.query);
        req.query = JSON.parse(req.query.query);

        // var mysqlCMD = "";
        // if(req.query.ProductName && req.query.ProductBrand){
        //     mysqlCMD = "SELECT * FROM grocery_list where name LIKE '%" + req.query.ProductName +"%' OR brand LIKE '%" + 
        //     req.query.ProductBrand + "%' order by name " + req.query.nameSort + ", brand " + req.query.brandSort + " limit 20";
        // }else if(req.query.ProductName){
        //     mysqlCMD = "SELECT * FROM grocery_list where name LIKE '%" + req.query.ProductName +"%' order by name " +
        //     req.query.nameSort + ", brand " + req.query.brandSort + " limit 20";
        // }else if(req.query.ProductBrand){
        //     mysqlCMD = "SELECT * FROM grocery_list where name LIKE '%" + req.query.ProductBrand +"%' order by name " + 
        //     req.query.nameSort + ", brand " + req.query.brandSort + " limit 20";
        // }
        // console.log(mysqlCMD);

        var mysqlCMD = [];
        if(req.query.ProductName){
            mysqlCMD.push({ name: { $like: "%" + req.query.ProductName + "%" } });
        }
        if(req.query.ProductBrand){
            mysqlCMD.push({ brand: { $like: "%" + req.query.ProductBrand + "%" } });
        }
        console.log(mysqlCMD);

        db.Grocery_list
            .findAll({
                where: {
                    $or: mysqlCMD
                    // [
                        // %% cannot be empty !!!!!!!
                        // { name: { $like: "%" + req.query.ProductName + "%" } },
                        // { brand: { $like: "%" + req.query.ProductBrand + "%" } }
                    // ]
                },
                limit: 20,
                subQuery: false,
                order: [
                    ["brand", req.query.brandSort],
                    ["name", req.query.nameSort],
                    // the last field is the primary / final sorted field
                ],
            })
            .then(function (result) {
                res
                    .status(200)
                    .json(result);
            })
            .catch(function (err) {
                console.log("error clause: " + err);
                res
                    .status(500)
                    .json(err);
            });

    }
}

var retrieveOne = function (db) {
    return function (req, res) {
        // Sequelize db calls OR Upload OR normal functions
        console.log("Retrieving Item");
        console.log(req.query);
        req.query = JSON.parse(req.query.query);
        db.Grocery_list
            .find({
                where:{
                    id: req.query
                }
            })
            .then(function (result) {
                res
                    .status(200)
                    .json(result);
            })
            .catch(function (err) {
                console.log("error clause: " + err);
                res
                    .status(500)
                    .json(err);
            });
    }
}

var updateOne = function (db) {
    return function (req, res) {
        // Sequelize db calls OR Upload OR normal functions
        console.log("Updating Item");
        console.log(req.body);
        db.Grocery_list
            .update({
                name: req.body.item.name,
                brand: req.body.item.brand,
                upc12: req.body.item.upc12
            },{
                where:{
                    id: req.body.item.id
                }
            })
            .then(function (result) {
                res
                    .status(200)
                    .json(result);
            })
            .catch(function (err) {
                console.log("error clause: " + err);
                res
                    .status(500)
                    .json(err);
            });
    }
}

module.exports = function (db) {
    return {
        retrieve: retrieve(db),
        retrieveOne: retrieveOne(db),
        updateOne: updateOne(db),
    }
};