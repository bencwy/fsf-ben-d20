module.exports = function (sequelize, DataTypes) {
    return sequelize.define("grocery_list", {
        // DEFINITIONS
        /* 
        <field name>: {
        type: DataTypes.<type>(<length>),
        - other parameters
        }, 
        */
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
        },
        upc12: {
            type: DataTypes.BIGINT(12),
            allowNull: false,
        },
        brand: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        }
    }, {
        // OPTIONS
        // don't add timestamps attributes updatedAt and createdAt
        timestamps: false,
        freezeTableName: true
    });
};
