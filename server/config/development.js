'use strict';

module.exports = {
  domain_name: "http://localhost:3000",
  PORT: 3000,

  MYSQL_USERNAME: 'root',
  MYSQL_PASSWORD: '123456', // Enter root password
  MYSQL_HOSTNAME: 'localhost',
  MYSQL_PORT: 3306,
  MYSQL_LOGGING: console.log,

  version: '1.0.0'
};