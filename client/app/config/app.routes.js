(function () {
    angular
        .module("mainModule")
        .config(uiRoutesConfig);

    uiRoutesConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uiRoutesConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            // .state("<ui-sref value>", {
            //     url: "/<browser path>",
            //     templateUrl: "<path>",
            //     controller: "<Controller name>",
            //     controllerAs: "<controller name>"
            // });
            .state("search", {
                url: "/search",
                templateUrl: "/app/search/search.html",
                controller: "SearchCtrl",
                controllerAs: "ctrl"
            })
            .state("edit", {
                url: "/edit/:id",
                templateUrl: "/app/edit/edit.html",
                controller: "EditCtrl",
                controllerAs: "ctrl"
            });
        
        // $urlRouterProvider.otherwise("/<default ui-sref value>");
        $urlRouterProvider.otherwise("/search");
    }
})();