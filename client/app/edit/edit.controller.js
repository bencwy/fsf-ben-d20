(function () {
    // Attaches controller to the main module
    angular
        .module("mainModule")
        .controller("EditCtrl", EditCtrl);

    // Inject Services
    EditCtrl.$inject = ["Service","$stateParams","$state"];

    function EditCtrl(Service,$stateParams,$state) {
        var ctrl = this;

        ctrl.id = $stateParams.id;
        ctrl.item = {};

        ctrl.searchDBOne = searchDBOne;
        ctrl.updateItem = updateItem;
        ctrl.checkFields = checkFields;

        function searchDBOne(){
            Service
                .searchDBOne(ctrl.id)
                .then(function(result){
					console.log("Search success from Server");
					console.log(result);
					ctrl.item = result.data;
				})
				.catch(function(err){
					console.log("Search failure from Server");
				});
        }

        function updateItem(){
            Service
                .updateItem(ctrl.item)
                .then(function(result){
					console.log("Search success from Server");
					console.log(result);
                    $state.go("search");
				})
				.catch(function(err){
					console.log("Search failure from Server");
				});
        }

        function checkFields(){
            if(ctrl.item.name && ctrl.item.brand && (ctrl.item.upc12 != null)){
                return false;
            }
            return true;
        }

        if(ctrl.id){
            searchDBOne();
        }
    }
})();