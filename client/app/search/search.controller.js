(function(){
	// Attaches controller to the main module
	angular
		.module("mainModule")
		.controller("SearchCtrl", SearchCtrl);

    // Inject Services
	SearchCtrl.$inject = ["Service"];

	// Function Constructor
	function SearchCtrl(Service) {
	  	// controller model
	  	var ctrl = this;
	  	// Initialise values
	  	ctrl.query = {
			  ProductName: "",
			  ProductBrand: "",
			  nameSort: "ASC",
			  brandSort: "ASC"
		}
		ctrl.searchResults = [];

	  	// Initialise functions
	  	ctrl.queryFill = queryFill;
		ctrl.searchDB = searchDB;
		ctrl.sortName = sortName;
		ctrl.sortBrand = sortBrand;

	  	function queryFill(){
	  		if(ctrl.query.ProductName || ctrl.query.ProductBrand){
				return false;
			}
			return true;
	  	};

		function searchDB(){
			Service
				.searchDB(ctrl.query)
				.then(function(result){
					console.log("Search success from Server");
					console.log(result);
					ctrl.searchResults = result.data;
				})
				.catch(function(err){
					console.log("Search failure from Server");
				});
		}

		function sortName(){
			if(ctrl.query.nameSort == "ASC"){
				ctrl.query.nameSort = "DESC";
			}
			else if(ctrl.query.nameSort == "DESC"){
				ctrl.query.nameSort = "ASC";
			}
			console.log("Name Sort: " + ctrl.query.nameSort);
			ctrl.searchDB();
		}

		function sortBrand(){
			if(ctrl.query.brandSort == "ASC"){
				ctrl.query.brandSort = "DESC";
			}
			else if(ctrl.query.brandSort == "DESC"){
				ctrl.query.brandSort = "ASC";
			}
			console.log("Brand Sort: " + ctrl.query.brandSort);
			ctrl.searchDB();
		}
	}
	
})();