(function () {
    // Attaches DeptService service to the DMS module
    angular
        .module("mainModule")
        .service("Service", Service);

    Service.$inject = ["$http"];

    function Service($http){
    	var service = this;

        service.searchDB = searchDB;
        service.searchDBOne = searchDBOne;
        service.updateItem = updateItem;
        
        function searchDB(query) {
            return $http({
                method: 'GET',
                url: 'api/grocery_list',
                params: {
                    query: query,
                }
            });
        }

        function searchDBOne(query) {
            return $http({
                method: 'GET',
                url: 'api/grocery_list_one',
                params: {
                    query: query,
                }
            });
        }

        function updateItem(item){
            return $http({
                method: 'POST',
                url: '/api/grocery_list',
                data: {
                    item : item
                }
            })
        }
    }

})();